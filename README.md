# Ninja Cookie

[![Ninja Cookie](resources/logo.png)](https://ninja-cookie.com/)

A WebExtension that takes care of the cookie banners for you.
Compatible with Chrome, Firefox, Opera and Edge.

## Prerequisites

```bash
# npm install -g webextension-toolbox
```

## Build

```bash
$ npm install
$ webextension-toolbox build <vendor>
```

Where `<vendor>` is one of `chrome`, `firefox`, `opera` and `edge`.

## Install

[![Install for Google Chrome](resources/chrome.png)](https://chrome.google.com/webstore/detail/ninja-cookie/jifeafcpcjjgnlcnkffmeegehmnmkefl)
[![Install for Firefox](resources/firefox.png)](https://chrome.google.com/webstore/detail/ninja-cookie/jifeafcpcjjgnlcnkffmeegehmnmkefl)
[![Install for Edge](resources/edge.png)](https://microsoftedge.microsoft.com/addons/detail/ninja-cookie/eaiglkmcbamilakcbmpgjfoemjhbonpe)
[![Install for Opera](resources/opera.png)](https://chrome.google.com/webstore/detail/ninja-cookie/jifeafcpcjjgnlcnkffmeegehmnmkefl)

## License

This work is dual-licensed under Prorietary license and GPL 3.0 (or any later version).
You can choose between one of them if you use this work.

`SPDX-License-Identifier: Propietary OR GPL-3.0-or-later`
