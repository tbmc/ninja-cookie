/*!
 * Copyright (c) 2020-present Théo Goudout. All rights reserved.
 *
 * This work is dual-licensed under Prorietary license and
 * GPL 3.0 (or any later version). You can choose between
 * one of them if you use this work.
 *
 * For details please refer to LICENSE file.
 */
const $ = require("jquery");

(async () => {
  const tabId = window.tabId;

  function updateDisableHostname(url) {
    // Setup disable hostname button
    const hostname = (new URL(url)).hostname.replace(/^www\./, '');
    $("#disable .dropdown-item[data-target='hostname']").text(
      // @desc The disable option for a specific website
      // @param hostname=google.com
      browser.i18n.getMessage('popupDisableHostname', [
        hostname,
      ])
    ).attr(
      'data-hostname',
      hostname
    ).toggleClass(
      'd-none',
      false
    );
  }

  function update(state) {
    switch (state.name) {
      case "disabled":
        // Tab is disabled
        $("#disable").removeClass("d-block").addClass("d-none");
        $("#enable").removeClass("d-none").addClass("d-block");
        break;
      case "pending":
      case "matched":
      case "done":
      case "info":
      case "warning":
      case "failed":
        // Tab is enabled
        updateDisableHostname(state.metadata.url);
      case "default":
        $("#disable").removeClass("d-none").addClass("d-block");
        $("#enable").removeClass("d-block").addClass("d-none");
        break;
      default:
        // Unknown state => hide buttons
        $("#disable").removeClass("d-block").addClass("d-none");
        $("#enable").removeClass("d-block").addClass("d-none");
        break;
    }
  }

  // Listen to update events
  window.background.addListener('state.set', data => {
    if (tabId !== data.tabId)
      return;

    update(data.newState);
  });

  // Initialize state
  update(await window.background.sendMessage({
    type: 'state.get',
    data: tabId,
  }));

  // Setup enabledisable buttons
  async function disable(args) {
    event.preventDefault();

    await window.background.sendEvent({
      type: 'state.disable',
      data: {
        ...args,
        id: tabId,
      }
    });
  }
  async function enable() {
    event.preventDefault();

    await window.background.sendEvent({
      type: 'state.enable',
      data: {
        id: tabId,
      }
    });
  }
  $("#disable .dropdown-item").click(
    async event => await disable($(event.target).data())
  );
  $("#enable button").click(enable);
})();
