/*!
 * Copyright (c) 2020-present Théo Goudout. All rights reserved.
 *
 * This work is dual-licensed under Prorietary license and
 * GPL 3.0 (or any later version). You can choose between
 * one of them if you use this work.
 *
 * For details please refer to LICENSE file.
 */
const $ = require("jquery");

(async () => {
  function update(info) {
    // Setup premium features
    $('.premium').toggleClass('disabled', info.status !== "active");
    $('#subscription').toggleClass('d-none', info.status === "active");
  }

  // Setup listener
  window.background.addListener('storage.set', async data => {
    if (!('licenseKey' in (data.options || {})))
      return;

    update(await window.background.sendMessage({
      type: 'license.get',
      data: {
        what: 'info',
      },
    }));
  });

  // Init license info
  update(await window.background.sendMessage({
    type: 'license.get',
    data: {
      what: 'info',
    },
  }));
})();
