/*!
 * Copyright (c) 2020-present Théo Goudout. All rights reserved.
 *
 * This work is dual-licensed under Prorietary license and
 * GPL 3.0 (or any later version). You can choose between
 * one of them if you use this work.
 *
 * For details please refer to LICENSE file.
 */
const $ = require("jquery");

(() => {
  // Setup one value
  function valueSet({container, value}) {
    const elem = $('#' + container);
    if (!elem.length) {
      return;
    }

    let input = elem.find('input[type="checkbox"]');
    if (input.length) {
      input.prop('checked', !!value);
    } else if ((input = elem.find('select')).length) {
      input.find("option[value='" + value + "']").prop('selected', 'selected');
    }
  }

  // Get container value to save
  function valueGet({container}) {
    const elem = $('#' + container);
    if (!elem.length) {
      return;
    }

    let input = elem.find('input[type="checkbox"]');
    if (input.length) {
      return input.prop('checked');
    } else if ((input = elem.find(':input')).length) {
      return input.val();
    }
  }

  function fieldSetup(container) {
    return {
      get: () => valueGet({container}),
      set: async (value) => {
        valueSet({
          container,
          value,
        })
        return $('#' + container);
      },
      reset: true,
    };
  }

  // Option name to container converter
  const containers = {
    'sticker': 'sticker',
    'stickerPosition': 'sticker-position',
    'show': 'live',
    'blocker': 'ab-lists-enabled',
    'trustAll': 'trust-all',
    'log': 'log',
  };

  window.Fields = {
    ...(window.Fields || {}),
    ...Object.fromEntries(Object.entries(containers).map(
      ([key, container]) => [key, fieldSetup(container)]
    ))
  };


  // // Setup premium options
  // async function premiumOptionsSetup({premiumOptions = null, ...args} = {}) {
  //   premiumOptions = premiumOptions || await License.getPremiumOptions(args);
  //   Object.entries(premiumOptions).map(
  //     ([key, value]) => valueSetup({
  //       container: containers[key],
  //       value: value,
  //       premium: true,
  //     })
  //   );

  //   // Set input disable state
  //   const data = await License.getInfo(args);
  //   $('.premium').toggleClass('disabled', data.status !== "active");
  //   $('.premium .col:last-of-type').attr(
  //     'data-tooltip', browser.i18n.getMessage('settingsPremiumTooltip'));
  //   $('.premium :input').prop('disabled', data.status !== "active");
  // }
})();
