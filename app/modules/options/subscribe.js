/*!
 * Copyright (c) 2020-present Théo Goudout. All rights reserved.
 *
 * This work is dual-licensed under Prorietary license and
 * GPL 3.0 (or any later version). You can choose between
 * one of them if you use this work.
 *
 * For details please refer to LICENSE file.
 */
const $ = require("jquery");

// Load bootstrap alert function
require('./bootstrap.js');

(async () => {
  // Escape string for HTML context
  function escapeHtml(unsafe) {
    return unsafe
         .replace(/&/g, "&amp;")
         .replace(/</g, "&lt;")
         .replace(/>/g, "&gt;")
         .replace(/"/g, "&quot;")
         .replace(/'/g, "&#039;");
  }

  // Handler function that takes care of queued subscriptions
  const handler = async () => {
    // Wait for values to be initialized
    await new Promise(resolve => {
      document.addEventListener('values.set', resolve, {once: true});
    });

    while (true) {
      const subscription = await window.background.sendMessage({
        type: 'subscribe.shift',
      });
      if (!subscription)
        break;

      switch (subscription.type) {
        case 'license':
          // Switch to License tab
          $('.nav[role="tablist"] a[href="#premium-tab"]').click();

          // Subscribe to License
          alertMessage(
            // @desc The message shown after a license key has been added through subscription process
            // @param license=0123456-789ABCD-EF01234
            browser.i18n.getMessage('settingsSubscriptionLicenseKey', [
              escapeHtml(subscription.license),
            ]),
            'info'
          );
          $('#license-key input').val(subscription.license);
          $('#license-key button').click();
          break;

        case 'ninja-cookie':
          // Switch to advanced tab
          $('.nav[role="tablist"] a[href="#advanced-tab"]').click();

          // Subscribe to Ninja Cookie list
          alertMessage(
            // @desc The message shown after a Ninja Cookie custom list has been added through subscription process
            // @param url=https://ninja-cookie.gitlab.io/rules/cmp.json
            browser.i18n.getMessage('settingsSubscriptionNCListsCustom', [
              escapeHtml(subscription.url)
            ]),
            'info'
          );
          $('#nc-lists-custom input').val(subscription.url);
          $('#nc-lists-custom button').click();
          break;

        case 'ad-block':
          // Switch to advanced tab
          $('.nav[role="tablist"] a[href="#advanced-tab"]').click();

          // Subscribe to Ad Block list
          alertMessage(
            // @desc The message shown after a Ad Blocker custom list has been added through subscription process
            // @param url=https://www.fanboy.co.nz/fanboy-cookiemonster.txt
            browser.i18n.getMessage('settingsSubscriptionABListsCustom', [
              escapeHtml(subscription.url),
            ]),
            'info'
          );
          $('#ab-lists-custom input').val(subscription.url);
          $('#ab-lists-custom button').click();
          break;
      }
    }
  };

  // Setup message handler for future subscriptions
  window.background.addListener('subscribe.add', event => {
    return handler();
  });

  // Start subscription handling
  handler();
})();
