/*!
 * Copyright (c) 2020-present Théo Goudout. All rights reserved.
 *
 * This work is dual-licensed under Prorietary license and
 * GPL 3.0 (or any later version). You can choose between
 * one of them if you use this work.
 *
 * For details please refer to LICENSE file.
 */
// Add "polyfill" for internal messaging interface
(async () => {
  if (!browser.runtime.onMessageInternal) {

    // Add listeners handler
    const loadListeners = [];
    browser.runtime.onLoadInternal = {
      addListener: function(listener) {
        loadListeners.push(listener);
      },
      removeListener: function(listener) {
        loadListeners.splice(loadListeners.indexOf(listener), 1);
      },
      hasListener: function(listener) {
        return loadListeners.includes(listener);
      },
    };
    const messageListeners = [];
    browser.runtime.onMessageInternal = {
      addListener: function(listener) {
        messageListeners.push(listener);
      },
      removeListener: function(listener) {
        messageListeners.splice(messageListeners.indexOf(listener), 1);
      },
      hasListener: function(listener) {
        return messageListeners.includes(listener);
      },
    };
    const unloadListeners = [];
    browser.runtime.onUnloadInternal = {
      addListener: function(listener) {
        unloadListeners.push(listener);
      },
      removeListener: function(listener) {
        unloadListeners.splice(unloadListeners.indexOf(listener), 1);
      },
      hasListener: function(listener) {
        return unloadListeners.includes(listener);
      },
    };

    // Keep reference to original function
    const sendMessage = browser.runtime.sendMessage;

    // Add callbacks handler
    const callbacks = {};
    browser.runtime.sendMessage = function(
      target,
      message,
      {queue = true} = {}
    ) {
      // Get iframe window
      if (target instanceof HTMLIFrameElement) {
        target = target.contentWindow;
      }

      // Check target type
      if (target.self === target) {
        // Check if a message should be sent
        const frameId = windows.findIndex(win => target === win);
        if (frameId !== -1 || queue) {
          return new Promise ((resolve, reject) => {
            const uid = Math.random().toString(16).slice(2);
            const data = {
              type: 'request',
              uid,
              message,
            };
            callbacks[uid] = {resolve, reject};
            if (frameId !== -1) {
              // Send message to window
              sendMessage({
                type: 'internal.forward',
                message: data,
                frameId,
              });
            } else {
              // Queue message for window
              queuedMessages.push({data, target});
            }
          });
        } else {
          throw 'Target not loaded';
        }
      }

      // Fallback
      return sendMessage.apply(this, arguments);
    }

    // Add messages handler
    let windows = [];
    let queuedMessages = [];
    async function messageLoad(data, source) {
      // Retrieve frame data associated to message source
      const frameData = await sendMessage({
        type: 'internal.frameData',
        nonce: data.nonce,
      });
      if (!frameData) return;

      // Add frameId to valid targets
      windows[frameData.frameId] = source;

      // Call onLoad listeners
      loadListeners.forEach(listener => listener({
        source,
        frameId: frameData.frameId,
      }));

      // Check queued messages for loaded window
      queuedMessages = queuedMessages.filter(message => {
        if (message.target === source) {
          // Send queued messages
          sendMessage({
            type: 'internal.forward',
            message: message.data,
            frameId: frameData.frameId,
          });
          return false;
        } else {
          return true;
        }
      });
    }
    function messageUnload(data) {
      const source = windows[data.frameId];

      // Remove queued messages for unloaded window
      queuedMessages = queuedMessages.filter(
        message => message.target !== source
      );

      // Call onUnload listeners
      unloadListeners.forEach(listener => listener({
        source,
        frameId: data.frameId,
      }));

      // Remove frameId from valid targets
      delete windows[data.frameId];
    }
    function messageRequest(data) {
      // Call listeners and return first response
      return Promise.race(messageListeners.map(async listener => {
        return new Promise(resolve => {
          let hasReplied;
          const sendResponse = r => { hasReplied = true; resolve(r); };
          const result = listener(data.message, {
            source: windows[data.frameId],
            frameId: data.frameId,
          }, sendResponse);
          if (hasReplied === true) // Provided a synchronous reply
            return;
          else if (result === true) // Will provide an asynchronous reply
            return;
          else // Has provided a reply (might be asynchronous)
            return resolve(result);
        });
      })).then(
        message => sendMessage({
          type: 'internal.forward',
          message: {
            type: 'resolve',
            uid: data.uid,
            message,
          },
          frameId: data.frameId,
        }),
        message => sendMessage({
          type: 'internal.forward',
          message: {
            type: 'reject',
            uid: data.uid,
            message,
          },
          frameId: data.frameId,
        }),
      );
    }
    function messageReply(data) {
      // Check for callback
      if (!(data.uid in callbacks)) {
        throw 'Unhandled reply';
      }

      // Retrieve callback
      const callback = callbacks[data.uid];
      delete callbacks[data.uid];

      // Execute callback
      return callback[data.type](data.message);
    }
    // Handle loading/unloading messages through 'window.postMessage'
    // This is used to associate a DOM frame to a webext frameId
    window.addEventListener('message', async ({data, source}) => {
      if (!data || typeof data !== 'object')
        return;

      // Check if the message is for us
      if (data.webext !== browser.runtime.id)
        return;

      switch (data.type) {
        case 'load':
          return messageLoad(data, source);
      }
    });
    // Handle request/reply messages through 'runtime.sendMessage'
    // This is used to avoid security issues
    browser.runtime.onMessage.addListener((data, sender) => {
      if (!data || typeof data !== 'object')
        return;

      switch (data.type) {
        case 'unload':
          return messageUnload(data);
        case 'request':
          return messageRequest(data);
        case 'resolve':
        case 'reject':
          return messageReply(data);
      }
    });

    // Setup frameId and parentFrameId for internal communication
    const internals = await sendMessage({type: 'internal.setup'});

    // Send load/unload message to parent/opener window
    if (window.opener || window.parent !== window.self) {
      const win = window.opener || window.parent;
      windows[internals.parentFrameId] = win;
      win.postMessage({
        webext: browser.runtime.id,
        type: 'load',
        nonce: internals.nonce,
      }, '*');
      window.addEventListener('beforeunload', () => {
        sendMessage({
          type: 'internal.forward',
          message: {
            type: 'unload',
          },
          frameId: internals.parentFrameId,
        });
      });
      window.addEventListener('unload', () => {
        sendMessage({
          type: 'internal.forward',
          message: {
            type: 'unload',
          },
          frameId: internals.parentFrameId,
        });
      });
    }
  }
})();
