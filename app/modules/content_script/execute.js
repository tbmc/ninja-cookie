/*!
 * Copyright (c) 2020-present Théo Goudout. All rights reserved.
 *
 * This work is dual-licensed under Prorietary license and
 * GPL 3.0 (or any later version). You can choose between
 * one of them if you use this work.
 *
 * For details please refer to LICENSE file.
 */
const Actions = require('../actions.js');

require('./internal.js');

(() => {
  // Do not return result to background, since result might not be serializable
  const execute = async data => {
    await Actions.execute(data);
    return null;
  };

  if (window.self === window.top) {
    // Listen to message from background
    window.background.addListener('action.execute', execute);
    // Forward messages from internal frames to background
    browser.runtime.onMessageInternal.addListener(message => {
      if (message.type.startsWith('action.')) {
        return window.background.sendMessage(message);
      }
    });
  } else {
    // Listen to message from parent frame
    browser.runtime.onMessageInternal.addListener(({type, data}) => {
      if (type === 'action.execute') {
        return execute(data);
      }
    });
  }
})();
