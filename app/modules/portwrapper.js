/*!
 * Copyright (c) 2020-present Théo Goudout. All rights reserved.
 *
 * This work is dual-licensed under Prorietary license and
 * GPL 3.0 (or any later version). You can choose between
 * one of them if you use this work.
 *
 * For details please refer to LICENSE file.
 */
const EventTarget = require("./eventtarget.js");

// Store pending messages
window.pendingMessages = [];

module.exports = function(port, iface = new EventTarget()) {
  // Handle outgoing messages/events
  iface.sendEvent = function(message) {
    port.postMessage({
      ...message,
      kind: 'event',
    });
  }

  iface.sendMessage = function(message) {
    const uid = Math.random().toString(16).substr(2);
    return new Promise((resolve, reject) => {
      window.pendingMessages[uid] = {resolve, reject};
      port.postMessage({
        ...message,
        kind: 'message',
        uid,
      });
    });
  }

  // Handle incoming messages/events
  port.onMessage.addListener(async ({uid, kind, ...message}) => {
    switch (kind) {
      // Handle replies
      case 'resolve':
      case 'reject':
        const callback = window.pendingMessages[uid][kind];
        delete window.pendingMessages[uid];
        await callback(message.data);
        break;

      // Handle events
      case 'event':
        await EventTarget.prototype.sendMessage.call(iface, message, port.sender);
        break;

      // Handle messages and send replies
      case 'message':
        let reply;
        try {
          const data = await EventTarget.prototype.sendMessage.call(
            iface,
            message,
            port.sender
          );
          reply = {
            kind: 'resolve',
            uid,
            data,
          };
        } catch (err) {
          console.error(err);
          const data = JSON.parse(JSON.stringify(
            err,
            Object.getOwnPropertyNames(err)
          ));
          reply = {
            kind: 'reject',
            uid,
            data,
          }
        }
        await port.postMessage(reply);
        break;

      // Discard any other message
      default:
        break;
    }
  });

  return iface;
};
