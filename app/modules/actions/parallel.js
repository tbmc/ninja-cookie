/*!
 * Copyright (c) 2020-present Théo Goudout. All rights reserved.
 *
 * This work is dual-licensed under Prorietary license and
 * GPL 3.0 (or any later version). You can choose between
 * one of them if you use this work.
 *
 * For details please refer to LICENSE file.
 */
const Actions = require("../actions.js");

/**
 * @summary Execute a set of {@link Action}s in parallel.
 *
 * @typedef ParallelAction
 * @type {Action}
 * @extends {Action}
 * @property {string} type - Must be one of `parallel`, `thread`, `all`.
 * @property {(Action|Action[])} action - A set of {@link Action}s to be
 *   executed in parallel.
 */
Actions.register(['parallel', 'thread', 'all'], async ({action, selector}) => {
  await Promise.all(
    action.action.map(async action => {
      return await Actions.execute({action, selector});
    })
  );
}, {recursive: true});
