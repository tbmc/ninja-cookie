/*!
 * Copyright (c) 2020-present Théo Goudout. All rights reserved.
 *
 * This work is dual-licensed under Prorietary license and
 * GPL 3.0 (or any later version). You can choose between
 * one of them if you use this work.
 *
 * For details please refer to LICENSE file.
 */
const Actions = require("../actions.js");

/**
 * @summary Wait for a JavaScript event to occur.
 *
 * @typedef EventAction
 * @type {Action}
 * @extends {Action}
 * @property {string} type - Must be one of `event`, `on`.
 * @property {number} [timeout=10000] - The time, in milliseconds,
 *   to wait for the expected event to occur.
 * @property {string} event - The name of the expected event.
 */
Actions.register(['event', 'on'], async ({action}) => {
  return new Promise((resolve, reject) => {
    const handler = event => {
      window.removeEventListener(action.event, handler);
      clearTimeout(timer);
      if (arguments.length)
        resolve();
      else
        reject({
          reason: browser.i18n.getMessage('eventTimeout'),
        });
    }
    const timeout = typeof action.timeout === "number" ? action.timeout : 10000;
    const timer = timeout ? setTimeout(handler, timeout) : null;
    window.addEventListener(action.event, handler);
  });
});
