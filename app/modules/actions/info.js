/*!
 * Copyright (c) 2020-present Théo Goudout. All rights reserved.
 *
 * This work is dual-licensed under Prorietary license and
 * GPL 3.0 (or any later version). You can choose between
 * one of them if you use this work.
 *
 * For details please refer to LICENSE file.
 */
const Actions = require("../actions.js");

/**
 * @summary Stop execution and set rule status to `info` or `warning`.
 *
 * @typedef InfoAction
 * @type {FailAction}
 * @extends {FailAction}
 * @property {string} type - Must be one of `info`, `warning`.
 * @property {string} [messageId] - **DEPECATED**
 *   The ID of a localized message.
 * @property {string} [message] - **DEPECATED**
 *   Another name for `reason`.
 */
Actions.register(['info', 'warning'], async ({action}) => {
  action.error = action.type;
  if (action.messageId)
    action.reason = browser.i18n.getMessage(action.messageId);
  if (action.message)
    action.reason = action.message;
  await Actions.fail({action});
});
