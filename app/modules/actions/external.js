/*!
 * Copyright (c) 2020-present Théo Goudout. All rights reserved.
 *
 * This work is dual-licensed under Prorietary license and
 * GPL 3.0 (or any later version). You can choose between
 * one of them if you use this work.
 *
 * For details please refer to LICENSE file.
 */
const Actions = require("../actions.js");

/**
 * @summary Execute one or more {@link Action}(s) in an external window.
 *
 * @typedef ExternalAction
 * @type {CheckAction}
 * @extends {CheckAction}
 * @property {string} type - Must be one of `external`, `window`.
 * @property {string} [url] - The URL that will be opened in the external
 *   window. If omitted, the `selector` parameter will be used to retrieve
 *   the URL.
 * @property {string} [selector] - A DOM selector for the anchor whose URL
 *   will be used as `url` parameter. It will be used as a fallback if the
 *   `url` parameter isn't specified. The first matched element will be used.
 * @property {string} [urlSelector] - **DEPRECATED**
 *   Another name for `selector`.
 * @property {number} [timeout=30000] - The time, in milliseconds,
 *   to wait for the expected anchor to appear.
 * @property {(Action|Action[])} action - One or more {@link Action}s to be
 *   executed in the external window.
 * @property {number} [executionTimeout=10000] - The time, in milliseconds,
 *   the external window has to execute the provided action(s). It is also the
 *   time the external window has to reach the first URL.
 *
 *   **NOTE**: For backward compatibility, if `executionTimeout` isn't provided,
 *   the `timeout` value will be used.
 * @property {boolean} [unloading=false] - Whether the external action should
 *   wait for the page to unload after the action(s) have been executed.
 * @property {number} [gracePeriod] - The time, in milliseconds, given to the
 *   external window to reload/process the action(s). The external window will
 *   be closed after this grace period.
 */
Actions.register(['external', 'window'], async ({action, selector, options, name, metadata}) => {
  // Get URL from DOM
  if (!action.url) {
    // Backward compatibility
    if (action.urlSelector) {
      selector = action.urlSelector;
    }
    const selected = await Actions.check({action, selector});
    action.url = selected.get(0).href;
  }

  // For backward compatibility
  if (typeof action.executionTimeout !== "number") {
    action.executionTimeout = action.timeout;
  }

  // Ask background to execute action in new window
  const message = {
    type: "action.external",
    data: {
      ...action,
      options: options,
      name: name + ' [' + action.type + ']',
      metadata: metadata,
    },
  };
  if (window.self === window.top) {
    return await window.background.sendMessage(message);
  } else {
    return await browser.runtime.sendMessage(window.top, message);
  }
}, {recursive: true});
