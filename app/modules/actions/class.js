/*!
 * Copyright (c) 2020-present Théo Goudout. All rights reserved.
 *
 * This work is dual-licensed under Prorietary license and
 * GPL 3.0 (or any later version). You can choose between
 * one of them if you use this work.
 *
 * For details please refer to LICENSE file.
 */
const Actions = require("../actions.js");

/**
 * @summary Toggle class(es) of the selected element(s).
 *
 * @typedef ClassAction
 * @type {CheckAction}
 * @extends {CheckAction}
 * @property {string} type - Must be `class`.
 * @property {number} [timeout=0] - The time, in milliseconds,
 *   to wait for the expected element(s) to appear.
 * @property {(string|string[])} className - One or more class names to toggle.
 * @property {boolean} [state=false] - The class(es) state.
 */
Actions.register(['class'], async ({action, selector}) => {
  if (typeof action.timeout !== "number") action.timeout = 0;
  const selected = await Actions.check({action, selector});
  selected.toggleClass(action.className, !!action.state);
  return selected;
});
