/*!
 * Copyright (c) 2020-present Théo Goudout. All rights reserved.
 *
 * This work is dual-licensed under Prorietary license and
 * GPL 3.0 (or any later version). You can choose between
 * one of them if you use this work.
 *
 * For details please refer to LICENSE file.
 */
const $ = require("jquery");
const Actions = require("../actions.js");

/**
 * @summary Simulate a mouse event on selected element(s).
 *
 * @typedef MouseEventAction
 * @type {CheckAction}
 * @extends {CheckAction}
 * @property {string} type - The type of event. Must be one of
 *   `click`, `dblclick`, `mouseup`, `mousedown`.
 * @property {number} [timeout=0] - The time, in milliseconds,
 *   to wait for the expected element(s) to appear.
 */
Actions.register(['click', 'dblclick', 'mouseup', 'mousedown'], async ({action, selector, options}) => {
  if (typeof action.timeout !== "number") action.timeout = 0;

  const selected = await Actions.check({action, selector});

  if (!!options.show) {
    selected.each((idx, elem) => $(elem).attr(
      'old-style', $(elem).attr('style') || ""
    ).attr(
      'style', ($(elem).attr('style') || "") + ';border:red solid 5px !important;'
    ));
    await new Promise(resolve => setTimeout(resolve, 100));
  }

  selected.each(function() {
    this.dispatchEvent(new MouseEvent(
      action.type, {view: window, bubbles: true, cancelable: true}
    ));
  });

  if (!!options.show) {
    selected.each((idx, elem) => $(elem).attr(
      'style', $(elem).attr('old-style') || ""
    ).removeAttr('old-style'));
  }

  return selected;
});
