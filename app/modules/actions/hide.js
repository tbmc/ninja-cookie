/*!
 * Copyright (c) 2020-present Théo Goudout. All rights reserved.
 *
 * This work is dual-licensed under Prorietary license and
 * GPL 3.0 (or any later version). You can choose between
 * one of them if you use this work.
 *
 * For details please refer to LICENSE file.
 */
const $ = require("jquery");
const Actions = require("../actions.js");

/**
 * @summary Hide the selected element(s).
 *
 * @description
 * The elements are hidden by overriding the inline style value.
 * The CSS `display` property is set to `none!important`.
 * The previous style value is saved and can be restored
 * using a {@link ShowAction}.
 *
 * @typedef HideAction
 * @type {CheckAction}
 * @extends {CheckAction}
 * @property {string} type - Must be `hide`.
 * @property {number} [timeout=0] - The time, in milliseconds,
 *   to wait for the expected element(s) to appear.
 */
Actions.register(['hide'], async ({action, selector, options}) => {
  if (typeof action.timeout !== "number") action.timeout = 0;
  const selected = await Actions.check({action, selector});
  if (options.show === "transparent") {
    selected.css('opacity', '0.75');
  } else if (!options.show) {
    selected.each((idx, elem) => $(elem).attr(
      'old-style', $(elem).attr('style') || ""
    ).attr(
      'style', ($(elem).attr('style') || "") + ';display:none!important;'
    ));
  }
  return selected;
});
