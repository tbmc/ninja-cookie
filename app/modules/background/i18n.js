/*!
 * Copyright (c) 2020-present Théo Goudout. All rights reserved.
 *
 * This work is dual-licensed under Prorietary license and
 * GPL 3.0 (or any later version). You can choose between
 * one of them if you use this work.
 *
 * For details please refer to LICENSE file.
 */

// This file is for translation detetction purpose.
// No code that is expected to run should be put there.
// It will be optimized out during minification.
(function() {
  const getMessage = str => str;

  // @desc Name of the extension
  getMessage('extensionName');
  // @desc Description of the extension
  getMessage('extensionDescription');
}) // Don't execute this function