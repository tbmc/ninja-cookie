/*!
 * Copyright (c) 2020-present Théo Goudout. All rights reserved.
 *
 * This work is dual-licensed under Prorietary license and
 * GPL 3.0 (or any later version). You can choose between
 * one of them if you use this work.
 *
 * For details please refer to LICENSE file.
 */
const License = require("../license.js");
const State = require("../state.js");
const Storage = require("../storage.js");

const compareVersions = require('compare-versions');

(async function() {
  // A list of known states
  const states = ['matched', 'done', 'info', 'warning', 'failed'];

  // The current holiday
  window.holiday = null;

  // Get the name of a holiday for a given date, or undefined
  function getHoliday(now = new Date()) {
    // Keep in mind that months starts from 0

    // Education day
    if (now.getMonth() === 0 && now.getDate() == 24)
      return {
        class: 'education-day',
        message: browser.i18n.getMessage('holidayEducationDay'),
      };

    // Data protection day
    if (now.getMonth() === 0 && now.getDate() == 28)
      return {
        class: 'data-protection-day',
        message: browser.i18n.getMessage('holidayDataProtectionDay'),
      };

    // Valentine's day
    if (now.getMonth() === 1 && now.getDate() == 14)
      return {
        class: 'valentines-day',
        message: browser.i18n.getMessage('holidayValentinesDay'),
      };

    // Women's day
    if (now.getMonth() === 2 && now.getDate() === 8)
      return {
        class: 'womens-day',
        message: browser.i18n.getMessage('holidayWomensDay'),
      };

    // Maybe Easter (it is surprisingly complicated to check)
    // if (now.getMonth() === 2 || now.getMonth() === 3) {
    //   // https://stackoverflow.com/questions/1284314/easter-date-in-javascript
    //   let Y = now.getFullYear();
    //   let C = Math.floor(Y/100);
    //   let N = Y - 19*Math.floor(Y/19);
    //   let K = Math.floor((C - 17)/25);
    //   let I = C - Math.floor(C/4) - Math.floor((C - K)/3) + 19*N + 15;
    //   I = I - 30*Math.floor((I/30));
    //   I = I - Math.floor(I/28)*(1 - Math.floor(I/28)*Math.floor(29/(I + 1))*Math.floor((21 - N)/11));
    //   let J = Y + Math.floor(Y/4) + I + 2 - C + Math.floor(C/4);
    //   J = J - 7*Math.floor(J/7);
    //   let L = I - J;
    //   let M = 3 + Math.floor((L + 40)/44);
    //   let D = L + 28 - 31*Math.floor(M/4);

    //   // Easter
    //   if (now.getMonth() + 1 === M && now.getDate() === D) {
    //     return {
    //     class: 'easter',
    //     message: browser.i18n.getMessage('holidayEaster'),
    //   };
    //   }
    // }

    // Labour day
    // if (now.getMonth() === 4 && now.getDate() === 1)
    //   return {
    //     class: 'labour-day',
    //     message: browser.i18n.getMessage('holidayLabourDay'),
    //   };

    // First day of summer
    // if (now.getMonth() === 5 && now.getDate() === 21)
    //   return {
    //     class: 'summer',
    //     message: browser.i18n.getMessage('holidaySummer'),
    //   };

    // Halloween
    // if ((now.getMonth() === 9 && now.getDate() === 31) || (now.getMonth() === 10 && now.getDate() === 1))
    //   return {
    //     class: 'halloween',
    //     message: browser.i18n.getMessage('holidayHalloween'),
    //   };

    // World AIDS Day
    // if (now.getMonth() === 11 && now.getDate() === 1)
    //   return {
    //     class: 'world-aids-day',
    //     message: browser.i18n.getMessage('holidayWorldAidsDay'),
    //   };

    // Christmas time (whole month of december but the 31st)
    if (now.getMonth() === 11 && now.getDate() <= 30)
      return {
        class: 'christmas',
        message: browser.i18n.getMessage('holidayChristmas'),
      };

    // New year's eve and new year
    if ((now.getMonth() === 11 && now.getDate() == 31) || (now.getMonth() === 0 && now.getDate() === 1))
      return {
        class: 'new-year',
        message: browser.i18n.getMessage('holidayNewYear'),
      };
  }

  // Holiday updater
  const holidayUpdate = async function() {
    holiday = getHoliday();

    // Update sticker holiday in all tabs
    const tabIds = Object.entries(State.getAll()).filter(
      ([k, v]) => states.includes(v.name)
    ).map(([k, v]) => +k);
    for (let i in tabIds) {
      await window.tabs.sendEvent(tabIds[i], {
        type: 'sticker.update',
        data: { holiday },
      });
    }
  };

  // Update holiday everyday at midnight
  const now = new Date();
  const midnight = new Date(now.getFullYear(), now.getMonth(), now.getDate() + 1);
  setTimeout(async function() {
    await holidayUpdate();
    setInterval(holidayUpdate, 1000 * 60 * 60 * 24); // 24hrs
  }, midnight - now + 1000); // Wait until 1sec after midnight to update holiday

  // Return the message ID of a message to display if any
  const isSafari = navigator.vendor && navigator.vendor.indexOf('Apple') > -1;
  async function getMessageId() {
    const internals = await Storage.getInternals();
    const messages = await Storage.getMessages();
    const isPremium = (await License.getInfo()).status === 'active';

    for (let id in messages) {
      const message = messages[id];
      if (message.promotion && (isPremium || isSafari))
        continue;

      switch (message.when) {
        case "install":
          // TODO
          // if (!options.welcome)
          //   break; // Backward compatibility
          if (internals.previousVersion)
            break; // An update occured
          if (message.lastDisplay)
            break; // The message has already been displayed
          return id;
        case "update":
          if (!internals.previousVersion)
            break; // No update occured yet
          if (message.lastDisplay >= internals.installDate)
            break; // The message has already been displayed
          if (compareVersions(internals.previousVersion, message.version) >= 0)
            break; // Previous version is too recent
          const manifest = browser.runtime.getManifest();
          let currentVersion = manifest.version;
          if (manifest.version_name) {
            currentVersion = manifest.version_name.substring(1);
          }
          if (compareVersions(message.version, currentVersion) > 0)
            break; // Current version is too old
          return id;
        default:
          if (message.lastDisplay && !message.recurring)
            break;
          const nextDisplay = new Date(message.lastDisplay || internals.installDate);
          switch(message.when.unit) {
            case 'year':
              nextDisplay.setFullYear(
                nextDisplay.getFullYear() + message.when.count
              );
              break;
            case 'month':
              nextDisplay.setMonth(
                nextDisplay.getMonth() + message.when.count
              );
              break;
            case 'day':
              nextDisplay.setDate(
                nextDisplay.getDate() + message.when.count
              );
              break;
          }
          if (Date.now() < nextDisplay)
            break; // Next display date is not reached yet
          return id;
      }
    }
  }

  // Listen to install/update events and store info
  browser.runtime.onInstalled.addListener(async ({reason, previousVersion}) => {
    switch (reason) {
      case "update":
        return await Storage.setInternals({
          data: {
            previousVersion,
            installDate: Date.now(),
          },
        });
    }
  });

  // Listen to messages from tabs
  window.tabs.addListener('sticker.hide_message', async (data, sender) => {
    // Update internals
    await Storage.setMessages({
      data: Object.fromEntries([[
        data.messageId,
        {lastDisplay: Date.now()},
      ]]),
    });

    // Remove welcome message from other tabs
    const tabIds = Object.entries(State.getAll()).filter(
      ([k, v]) => states.includes(v.name)
    ).map(([k, v]) => +k);
    for (let i in tabIds) {
      if (!sender.tab || tabIds[i] !== sender.tab.id) {
        await window.tabs.sendEvent(tabIds[i], {
          type: 'sticker.hide_message',
          data,
        });
      }
    }
  });

  // Update sticker when storage is updated
  window.background.addListener('storage.set', async data => {
    if (!('stickerPosition' in (data.options || {})))
      return;

    // Update sticker position in all tabs
    const tabIds = Object.entries(State.getAll()).filter(
      ([k, v]) => states.includes(v.name)
    ).map(([k, v]) => +k);
    for (let i in tabIds) {
      await window.tabs.sendEvent(tabIds[i], {
        type: 'sticker.update',
        data: {
          holiday,
          position: data.options['stickerPosition'],
        },
      });
    }
  });

  // Update sticker when state is updated
  window.background.addListener('state.set', async ({tabId, newState}) => {
    await Promise.all([
      (async () => {
        // Firefox for Android cannot set icons
        if (browser.browserAction.setIcon && newState.icons) {
          await browser.browserAction.setIcon({
            tabId: tabId,
            path: newState.icons,
          });
        }
      })(),
      (async () => {
        const options = await License.getOptions();
        if (!options.sticker) {
          return;
        }

        // Display message if necessary
        if (newState.name === 'pending') {
          const messageId = await getMessageId();
          if (messageId) {
            await window.tabs.sendEvent(tabId, {
              type: 'sticker.show_message',
              data: {
                state: 'pending',
                holiday,
                position: options['stickerPosition'],
                messageId,
              }
            });
          }
        }
      })(),
      (async () => {
        const options = await License.getOptions();
        if (!options.sticker) {
          return;
        }

        // Display sticker when transitionning to eligible states
        if (states.includes(newState.name)) {
          await window.tabs.sendEvent(tabId, {
            type: 'sticker.show',
            data: {
              state: newState.name,
              holiday,
              position: options['stickerPosition'],
            }
          });
        }
      })(),
    ]);
  });

  // Save install date if not set
  const internals = await Storage.getInternals();
  if (!internals.installDate)
    await Storage.setInternals({data: {
      installDate: Date.now(),
    }});

  // Update holiday now
  await holidayUpdate();
})();

// This is for translation detetction purpose.
// No code that is expected to run should be put there.
// It will be optimized out during minification.
(function() {
  const getMessage = str => str;

  getMessage('messageWelcome');
  getMessage('messageUpdate');
  getMessage('messagePremiumPromotionAfterAWeek');
  getMessage('messagePremiumPromotionEveryMonth');
}) // Don't execute this function
