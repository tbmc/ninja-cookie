/*!
 * Copyright (c) 2020-present Théo Goudout. All rights reserved.
 *
 * This work is dual-licensed under Prorietary license and
 * GPL 3.0 (or any later version). You can choose between
 * one of them if you use this work.
 *
 * For details please refer to LICENSE file.
 */
const Storage = require("../storage.js");
const State = require("../state.js");

(function() {
  // Method used to force i18n string detection
  const getMessage = s => s;

  // Define basic states
  State.define("matched", {
    // @desc The status text when Ninja Cookie is setting up cookie banners. > Blue dot
    "status": browser.i18n.getMessage('matchedStatus'),
    // @desc The description of the status when Ninja Cookie is setting up cookie banners. > Blue dot
    "desc": browser.i18n.getMessage('matchedStatusDesc'),
    "sticker": {
      "src": "/icons/matched-32.png",
      // @desc The matched status alternative text
      "alt": browser.i18n.getMessage('matchedStatusAlt'),
    },
    "icons": {
      "16": "icons/matched-16.png",
      "24": "icons/matched-24.png",
      "32": "icons/matched-32.png"
    },
    "report": {
      "category": 0,
      // @desc The default matched message displayed in the report form
      // @param url=https://google.com/
      // @param version=0.2.4
      // @param userAgent=Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36
      // @param matched=[]
      "messageId": getMessage('matchedReportMessage'),
    },
  });
  State.define("warning", {
    // @desc The text status when Ninja Cookie has hidden automatically a cookie banner. > Orange Dot
    "status": browser.i18n.getMessage('warningStatus'),
    // @desc The description of the message displayed when a cookie banner is detected but Ninja Cookie does not know how to set it up. The cookie banner is  hidden with the alternative rules. > Orange dot
    "desc": browser.i18n.getMessage('warningStatusDesc'),
    "sticker": {
      "src": "/icons/warning-32.png",
      // @desc The warning status alternative text > Orange dot
      "alt": browser.i18n.getMessage('warningStatusAlt'),
    },
    "icons": {
      "16": "icons/warning-16.png",
      "24": "icons/warning-24.png",
      "32": "icons/warning-32.png"
    },
    "report": {
      "category": 0,
      // @desc The default warning message displayed in the report form
      // @param url=https://google.com/
      // @param version=0.2.4
      // @param userAgent=Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36
      // @param matched=[]
      "messageId": getMessage('warningReportMessage'),
    },
  });
  State.define("done", {
    // @desc The text status when Ninja Cookie has correctly setup a cookie banner. > Green Dot
    "status": browser.i18n.getMessage('doneStatus'),
    // @desc The description of the status when Ninja Cookie has correctly setup a cookie banner. > Green Dot
    "desc": browser.i18n.getMessage('doneStatusDesc'),
    "sticker": {
      "src": "/icons/done-32.png",
      // @desc The done status alternative text > Green dot
      "alt": browser.i18n.getMessage('doneStatusAlt'),
    },
    "icons": {
      "16": "icons/done-16.png",
      "24": "icons/done-24.png",
      "32": "icons/done-32.png"
    },
    "report": {
      "category": 0,
      // @desc The default done message displayed in the report form
      // @param url=https://google.com/
      // @param version=0.2.4
      // @param userAgent=Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36
      // @param matched=[]
      "messageId": getMessage('doneReportMessage'),
    },
  });
  State.define("info", {
    // @desc The text status when Ninja Cookie cannot set a cookie banner. > Yellow Dot
    "status": browser.i18n.getMessage('infoStatus'),
    // @desc The description of the message displayed when a cookie banner cannot be set with Ninja Cookie rules. > Yellow dot
    "desc": browser.i18n.getMessage('infoStatusDesc'),
    "sticker": {
      "src": "/icons/info-32.png",
      // @desc The info status alternative text. > Yellow dot
      "alt": browser.i18n.getMessage('infoStatusAlt'),
    },
    "icons": {
      "16": "icons/info-16.png",
      "24": "icons/info-24.png",
      "32": "icons/info-32.png"
    },
    "report": {
      "category": 0,
      // @desc The default info message displayed in the report form
      // @param url=https://google.com/
      // @param version=0.2.4
      // @param userAgent=Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36
      // @param matched=[]
      "messageId": getMessage('infoReportMessage'),
    },
  });
  State.define("failed", {
    // @desc The failed reason text > Red dot
    "status": browser.i18n.getMessage('failedStatus'),
    // @desc The description of the text displayed when an error has occured. > Red dot
    "desc": browser.i18n.getMessage('failedStatusDesc'),
    "sticker": {
      "src": "/icons/failed-32.png",
      // @desc The failed status alternative text > Red dot
      "alt": browser.i18n.getMessage('failedStatusAlt'),
    },
    "icons": {
      "16": "icons/failed-16.png",
      "24": "icons/failed-24.png",
      "32": "icons/failed-32.png"
    },
    "report": {
      "category": 1,
      // @desc The default failed message displayed in the report form
      // @param url=https://google.com/
      // @param version=0.2.4
      // @param userAgent=Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36
      // @param matched=[]
      "messageId": getMessage('failedReportMessage'),
    },
  });
  State.define("default", {
    // @desc The status text when Ninja Cookie is not running in a tab.
    "status": browser.i18n.getMessage('defaultStatus'),
    // @desc The description of the status when Ninja Cookie is not running.
    "desc": browser.i18n.getMessage('defaultStatusDesc'),
    "sticker": {
      "src": "/icons/default-32.png",
      // @desc The default status alternative text
      "alt": browser.i18n.getMessage('defaultStatusAlt'),
    },
    "icons": {
      "16": "icons/default-16.png",
      "24": "icons/default-24.png",
      "32": "icons/default-32.png"
    },
    "report": {
      "category": 0,
      // @desc The default message displayed in the report form
      // @param url=https://google.com/
      // @param version=0.2.4
      // @param userAgent=Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36
      // @param matched=[]
      "messageId": getMessage('defaultReportMessage'),
    },
    "resetMetadata": true,
  }, (before, after, old) => {
    return old ? old : (before === 'default' ? after : 'default')
  });
  State.define("pending", {
    // @desc The status text when Ninja Cookie has not detected any cookie banners. > No dot
    "status": browser.i18n.getMessage('pendingStatus'),
    // @desc The description of the status when Ninja Cookie has not detected any cookie banners.
    "desc": browser.i18n.getMessage('pendingStatusDesc'),
    "sticker": {
      "src": "/icons/pending-32.png",
      // @desc The pending status alternative text
      "alt": browser.i18n.getMessage('pendingStatusAlt'),
    },
    "icons": {
      "16": "icons/pending-16.png",
      "24": "icons/pending-24.png",
      "32": "icons/pending-32.png"
    },
    "report": {
      "category": 0,
      // @desc The default pending message displayed in the report form
      // @param url=https://google.com/
      // @param version=0.2.4
      // @param userAgent=Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36
      // @param matched=[]
      "messageId": getMessage('pendingReportMessage'),
    },
    "resetMetadata": true,
  }, (before, after, old) => {
    return old ? old : (before === 'pending' ? after : 'pending')
  });

  // Add/remove/update state when tab is opened/closed/unloaded
  browser.tabs.onCreated.addListener(async tab => await State.set(tab.id, {
    url: tab.url
  }));
  browser.tabs.onRemoved.addListener(State.remove);
  if (browser.tabs.onReplaced) // Might not be supported on all browsers
    browser.tabs.onReplaced.addListener(State.replace);
  browser.webNavigation.onBeforeNavigate.addListener(async details => {
    if (details.frameId !== 0)
      return;

    // Only update known tabs info. In some cases, future tabs can trigger this
    // event (eg. using URL autocompletion)
    if (!(details.tabId in State.getAll()))
      return;

    await State.update(details.tabId, 'default', {url: details.url});
  });

  window.tabs.addListener('state.init', (data, sender) => {
    State.update(sender.tab.id, 'pending', {
      url: sender.url,
      initialized: true,
    });
  });

  window.tabs.addListener('state.update', async (data, sender) => {
    // Update stats if time is provided
    if (data.time) {
      const stats = await Storage.getStats();
      stats.totalTime += data.time;
      stats.totalBanner ++;
      await Storage.setStats({data: stats});
    }
    // Update name if error is provided
    if (data.error)
      if (['info', 'warning'].includes(data.error.name))
        data.name = data.error.name;
    // Update state
    return await State.update(sender.tab.id, data.name, {
      ...data.metadata,
      url: sender.url
    });
  });
})();
