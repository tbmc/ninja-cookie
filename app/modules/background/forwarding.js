/*!
 * Copyright (c) 2020-present Théo Goudout. All rights reserved.
 *
 * This work is dual-licensed under Prorietary license and
 * GPL 3.0 (or any later version). You can choose between
 * one of them if you use this work.
 *
 * For details please refer to LICENSE file.
 */
const License = require("../license.js");
const Rules = require("../rules.js");
const State = require("../state.js");
const Storage = require("../storage.js");

// Get information from background
[window.popups, window.options].forEach(port => {
  port.addListener('holiday.get', () => {
    return window.holiday;
  });

  port.addListener('license.get', data => {
    let what = data.what ? data.what[0].toUpperCase() + data.what.substring(1) : '';
    return License['get' + what](data);
  });
  port.addListener('license.set', data => {
    let what = data.what ? data.what[0].toUpperCase() + data.what.substring(1) : '';
    return License['set' + what](data);
  });
  port.addListener('license.reset', data => {
    let what = data.what ? data.what[0].toUpperCase() + data.what.substring(1) : '';
    return License['reset' + what](data);
  });

  port.addListener('rules.get', data => {
    let what = data.what ? data.what[0].toUpperCase() + data.what.substring(1) : '';
    return Rules['get' + what](data);
  });

  port.addListener('state.get', State.get);

  port.addListener('storage.get', data => {
    let what = data.what ? data.what[0].toUpperCase() + data.what.substring(1) : '';
    return Storage['get' + what](data);
  });
  port.addListener('storage.set', data => {
    let what = data.what ? data.what[0].toUpperCase() + data.what.substring(1) : '';
    return Storage['set' + what](data);
  });
  port.addListener('storage.reset', data => {
    let what = data.what ? data.what[0].toUpperCase() + data.what.substring(1) : '';
    return Storage['reset' + what](data);
  });

  port.addListener('subscribe.shift', () => {
    return window.Subscriptions.shift();
  });
});

// Forward events to popup
window.background.addListener('state.set', (data, sender, type) => {
  window.popups.sendEvent(data.tabId, {type, data});
});
window.background.addListener('storage.set', (data, sender, type) => {
  window.popupPorts.forEach(
    port => port.sendEvent({type, data})
  );
});

// Open options from background
window.popups.addListener('options.open', () => {
  browser.runtime.openOptionsPage();
});

// Forward events to options page
window.background.addListener('subscribe.add', (data, sender, type) => {
  window.options.sendEvent({type, data});
});

// Get information from background
