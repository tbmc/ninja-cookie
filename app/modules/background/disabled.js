/*!
 * Copyright (c) 2020-present Théo Goudout. All rights reserved.
 *
 * This work is dual-licensed under Prorietary license and
 * GPL 3.0 (or any later version). You can choose between
 * one of them if you use this work.
 *
 * For details please refer to LICENSE file.
 */
const License = require("../license.js");
const State = require("../state.js");

// Wait for basic states
require('./state.js');

(async () => {
  // Method used to force i18n string detection
  const getMessage = s => s;

  // Define disabled state
  State.define("disabled", {
    // @desc The disabled status text
    "status": browser.i18n.getMessage('disabledStatus'),
    // @desc The disabled status description
    "desc": browser.i18n.getMessage('disabledStatusDesc'),
    "sticker": {
      "src": "/icons/disabled-32.png",
      // @desc The disabled status alternative text
      "alt": browser.i18n.getMessage('disabledStatusAlt'),
    },
    "icons": {
      "16": "icons/disabled-16.png",
      "24": "icons/disabled-24.png",
      "32": "icons/disabled-32.png"
    },
    "report": {
      "category": 0,
      // @desc The default disabled message displayed in the report form
      // @param url=https://google.com/
      // @param version=0.2.4
      // @param userAgent=Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36
      "messageId": getMessage('disabledReportMessage'),
    },
  }, (before, after, old) => {
    if (old) return old;

    // Don't override unknown states
    const states = ['pending', 'default', 'matched', 'warning', 'done', 'info', 'failed'];
    if (!states.includes(after))
      return after;
    if (!states.includes(before))
      return before;

    return 'disabled';
  });


  async function disableForDuration(tabId, duration) {
    // Check if we should disable
    if (![30].includes(duration)) {
      const info = await License.getInfo();
      if (info.status !== "active") {
        return;
      }
    }

    let timer;
    if (Number.isFinite(duration)) {
      duration *= 1000 * 60; // Duration in minutes -> milliseconds
      timer = setTimeout(async () => {
        const state = State.get(tabId);
        if (state.name !== "disabled" || state.metadata.reason !== "duration")
          return;

        enableForDuration(tabId, state);
      }, duration);
    }

    // Update state to disabled
    return await State.update(tabId, 'disabled', {
      reason: "duration",
      timer,
    });
  }

  async function disableForHostname(tabId, hostname) {
    // Check if we should disable
    const info = await License.getInfo();
    if (info.status !== "active")
      return;

    // Add hostname to whitelist
    const options = await License.getPremiumOptions();
    const whitelist = new Set(options.whitelist);
    whitelist.add(hostname);
    await License.setPremiumOptions({data: {whitelist: [...whitelist]}});

    // Update state to disabled
    return await State.update(tabId, 'disabled', {
      reason: "hostname",
      hostname,
    });
  }

  async function enableForDuration(tabId, state) {
    if (state.metadata.timer) {
      clearTimeout(state.metadata.timer);
      delete state.metadata.reason;
      delete state.metadata.timer;
    }

    // Force state to pending
    return await State.set(tabId, 'pending', state.metadata);
  }

  async function enableForHostname(tabId, state) {
    // Remove hostname from whitelist
    const options = await License.getPremiumOptions();
    const whitelist = new Set(options.whitelist);
    whitelist.delete(state.metadata.hostname);
    await License.setPremiumOptions({data: {whitelist: [...whitelist]}});

    // State will be set to pending by event listener on Storage
    // return State.set(tabId, 'pending', state.metadata);
  }

  // Disable/Enable tab on popup request
  window.popups.addListener('state.disable', async data => {
    const state = State.get(data.id);
    if (state.name === "disabled")
      return;

    switch (data.target) {
      case "hostname":
        return await disableForHostname(data.id, data.hostname);
      case "duration":
        return await disableForDuration(data.id, data.duration);
    }
  });

  window.popups.addListener('state.enable', async data => {
    const state = State.get(data.id);
    if (state.name !== "disabled")
      return;

    switch (state.metadata.reason) {
      case "hostname":
        return await enableForHostname(data.id, state);
      case "duration":
        return await enableForDuration(data.id, state);
    }
  });

  // Prepare whitelist
  async function getWhitelistAsync() {
    const options = await License.getPremiumOptions();
    return new Set(options.whitelist);
  }

  // Initialize whitelist
  let whitelistAsync = getWhitelistAsync();

  // Update one tab
  async function update(tabId, url = null) {
    const state = State.get(tabId);
    if (!url && state.metadata)
        url = state.metadata.url;

    const whitelist = await whitelistAsync;
    const hostname = url ? (new URL(url)).hostname.replace(/^www\./, '') : null;

    switch (state.name) {
      case 'disabled':
        // Check if we should re-enable
        switch (state.metadata.reason) {
          case 'hostname':
            if (!hostname || !whitelist.has(hostname)) {
              // Force state to default
              await State.set(tabId, 'pending', {url});
            }
            break;
          case 'duration': // timeout is already set
          default:
            break;
        }
        break;

      case 'pending':
      case 'default':
      case 'matched':
      case 'warning':
      case 'done':
      case 'info':
      case 'failed':
        // Check if we should disable
        if (hostname && whitelist.has(hostname)) {
          // Update state to disabled
          await State.update(tabId, 'disabled', {
            reason: "hostname",
            hostname,
            url,
          });
        }
        break;
    }
  }

  // Update all tabs
  async function updateAll() {
    for (let tabId in State.getAll()) {
      await update(+tabId);
    }
  }

  // Update whitelist when storage/license is updated
  window.background.addListener('storage.set', async data => {
    if (!('whitelist' in (data.premiumOptions || {})))
      if (!('licenseKey' in (data.options || {})))
        return;

    whitelistAsync = getWhitelistAsync();
    await updateAll();
  });
  window.background.addListener('license.get', async ({defaultLicenseKey, info}) => {
    if (info.licenseKey !== defaultLicenseKey)
      return;

    whitelistAsync = getWhitelistAsync();
    await updateAll();
  });

  // Do nothing when disabled
  // window.background.addListener('state.set', ({tabId, newState}) => {
  //   switch (newState.name) {
  //     case 'disabled':
  //       break;
  //   }
  // });

  // Disable tabs whose URL is whitelisted
  browser.webNavigation.onBeforeNavigate.addListener(details => {
    if (details.frameId !== 0)
      return;

    // Only update known tabs info. In some cases, future tabs can trigger this
    // event (eg. using URL autocompletion)
    if (!(details.tabId in State.getAll()))
      return;

    return update(details.tabId, details.url);
  });
})();
