(async () => {
  const framesData = {};

  browser.runtime.onMessage.addListener(async ({type, ...data}, sender) => {
    switch (type) {
      case 'internal.setup':
        let nonce;
        if (sender.frameId !== 0) {
          nonce = Math.random().toString(16).slice(2);
          framesData[nonce] = {
            frameId: sender.frameId,
          };
        }

        const frames = await browser.webNavigation.getAllFrames({
          tabId: sender.tab.id,
        });
        return {
          frameId: sender.frameId,
          parentFrameId: frames.find(
            frame => frame.frameId === sender.frameId
          ).parentFrameId,
          nonce,
        };
      case 'internal.frameData':
        const frameData = framesData[data.nonce];
        if (frameData) {
          delete framesData[data.nonce];
          return frameData;
        }
        return;

      case 'internal.forward':
        return await browser.tabs.sendMessage(
          sender.tab.id,
          {
            ...data.message,
            frameId: sender.frameId,
          },
          { frameId: data.frameId }
        );
    }
  });
})();