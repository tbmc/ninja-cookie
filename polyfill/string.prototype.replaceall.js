/*!
 * Copyright (c) 2020-present Théo Goudout. All rights reserved.
 *
 * This work is dual-licensed under Prorietary license and
 * GPL 3.0 (or any later version). You can choose between
 * one of them if you use this work.
 *
 * For details please refer to LICENSE file.
 */
"use strict";

// From https://vanillajstoolkit.com/polyfills/stringreplaceall/
/**
 * String.prototype.replaceAll() polyfill
 * https://gomakethings.com/how-to-replace-a-section-of-a-string-with-another-one-with-vanilla-js/
 * @author Chris Ferdinandi
 * @license MIT
 */
if (!String.prototype.replaceAll) {
  Object.defineProperty(String.prototype, 'replaceAll', {
    value: function(str, newStr) {
      // If a regex pattern
      if (Object.prototype.toString.call(str).toLowerCase() === '[object regexp]') {
        return this.replace(str, newStr);
      }

      // If a string
      return this.replace(new RegExp(str, 'g'), newStr);
    },
  });
}
