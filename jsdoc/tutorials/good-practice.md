To write a "good" rule, there are a few thinks that are worth keeping in mind:

- Don't forget to **disable your webextensions**: an ad-blocker could modify a cookie banner behaviour;
- **Disable the other rules lists**, including the alternatives lists;
- **Enable the debug logs** in Ninja Cookie settings to get extra information in the JavaScript debugging console;
- Check the behaviour of the cookie banner when **accepting all** cookie usage and when **denying all** cookie usage manually: it will gve you a good idea of how the cookie banner is acting;
- Check the behaviour of the cookie banner **before** and **after** setting it up: is the cookie banner disappearing after a reload? Is the cookie banner appearing everytime?
- Try to **find another website** with the same cookie banner: a CMP can display a lot of different cookie banners types (banner at the bottom, full screen parameter window, etc.);
- Test your rule in **incognito mode** and in your **regular environment**;
- Try to **understand how the consent is saved**: if you know what the cookie banner does, you will know what to check;
- Try to **find information about the cookie banner**: it might give you information about the selectors to use (in a GitHub repository or a documentation for example);
- **Be flexible**: when writing a rule, one tends to way for it to finish executing before doing anything else, but in real-life users are not always as patient!


These are generic advices. You should try and find a way to be as efficient as possible when writing a robust rule. If you come up with something better than what we are currently doing, feel free to contact us and we will update this tutorial section.

