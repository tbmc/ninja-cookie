## Add a custom rules list

First, let's create a dummy rules list with the following content:

```json
{
  "metadata": {
    "name": "My first rule list",
    "version": "0.1",
    "bugs_email": "john.doe@gmail.com"
  }
}
```

Save it somewhere on your computer. You then need to add this rules list to your custom lists. To do so, go to the "Advanced" tabs of the Ninja Cookie settings, and add it as a custom list. To avoid any side-effect from another list, it is recommended to choose "None" for default lists.

![Advanced settings](advanced-settings-1.png)

Depending on your OS and browser, adding a local file can be as simple as using the `file://` protocol. If it is not working in your environement, the workaround is to setup a local http/ftp file server using:

- [Python](https://developer.mozilla.org/en-US/docs/Learn/Common_questions/set_up_a_local_testing_server#Running_a_simple_local_HTTP_server);
- [PHP](https://developer.mozilla.org/en-US/docs/Learn/Common_questions/set_up_a_local_testing_server#Running_server-side_languages_locally);
- [Apache](https://httpd.apache.org/);
- [NGINX](https://www.nginx.com/);
- [a browser extension](https://chrome.google.com/webstore/detail/web-server-for-chrome/ofhbbkphhbklhfoeikjpcbhemlocgigb?hl=en);
- or any other solution.

The file path will then look something like `http://127.0.0.1:8080/my-custom-list.json`.

Once added, your custom list will be shown as such:

![Advanced settings](advanced-settings-2.png)

**Don't forget to save your settings.**

We will now add a simple rule to the rules list:

```json
{
  ...
  "cmp/cookiebot": {
    "metadata": {
      "name": "Cookiebot",
      "website": "https://www.cookiebot.com/",
      "iab": "cookiebot.mgr.consensu.org"
    },

    "match": [
      { "type": "check", "selector": "#CybotCookiebotDialog" }
    ],
    "action": [
      { "type": "hide" },
      { "type": "remove", "selector": "#CybotCookiebotDialogBodyUnderlay", "optional": true },
      { "type": "sleep" },
      { "type": "checkbox", "selector": "#CybotCookiebotDialogBodyLevelButtonPreferences" },
      { "type": "checkbox", "selector": "#CybotCookiebotDialogBodyLevelButtonStatistics" },
      { "type": "checkbox", "selector": "#CybotCookiebotDialogBodyLevelButtonMarketing" },
      { "type": "sleep" },
      { "type": "click", "selector": "#CybotCookiebotDialogBodyLevelButtonAccept" }
    ]
  }
}
```

And once reloaded, your custom list is show as follow:

![Advanced settings](advanced-settings-3.png)

**Congratulations!** Your custom list is now added to Ninja Cookie and you can test it.

Next steps:
- [Write your first rule]{@tutorial first-rule}
- [write advanced rules]{@tutorial advanced-rule}
- [Check the good practices]{@tutorial good-practice}
- [Share your rules]{@tutorial share-rule}
