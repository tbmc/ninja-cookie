/*!
 * Copyright (c) 2020-present Théo Goudout. All rights reserved.
 *
 * This work is dual-licensed under Prorietary license and
 * GPL 3.0 (or any later version). You can choose between
 * one of them if you use this work.
 *
 * For details please refer to LICENSE file.
 */
(() => {
  // Log received messages
  [
    window.background,
    window.tabs,
    window.popups,
    window.options,
  ].filter(
    port => !!port
  ).forEach(
    port => port.addListener(
      '*',
      (data, sender, type) => console.debug(type, data, sender)
    )
  );

  // Listen to external messages
  const listeners = {};
  browser.runtime.onMessageExternal.addListener(async ({type, data}, sender) => {
    switch (type) {
      case 'state.update':
        console.debug('Registering external listener', sender.id, 'to state.update');
        listeners[sender.id] = (message) => browser.runtime.sendMessage(
          sender.id,
          message
        );
        return;
      case 'storage.set':
        let what = data.what ? data.what[0].toUpperCase() + data.what.substring(1) : '';
        return await Storage['set' + what](data);
      default:
        throw 'Message type is not handled';
    }
  });

  // Forward state update received from background to external listeners
  window.background.addListener('state.set', ({tabId, newState}) => {
    Object.entries(listeners).forEach(([id, listener]) => {
      try {
        console.debug('Send state.update to external listener', id);
        listener({
          ...newState,
          tabId,
        });
      } catch {}
    });
  });

})();
